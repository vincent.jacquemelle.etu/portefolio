Drop table if exists importAnglais;

CREATE TABLE importAnglais (
    n1 TEXT DEFAULT NULL,
    n2 TEXT DEFAULT NULL,
    n3 TEXT DEFAULT NULL,
    n4 TEXT DEFAULT NULL,
    n5 TEXT DEFAULT NULL,
    n6 TEXT DEFAULT NULL,
    n7 TEXT DEFAULT NULL
);

COPY importAnglais FROM './UCAS\ June\ deadline\ 2023\ -\ Domicile.csv' DELIMITER '';'' CSV HEADER;

Create TABLE q1a AS
    SELECT n1 As year, sum(n7) As nbApplicants
    FROM importAnglais,
    Group by n1; 

COPY q1a TO './q1a.csv' DELIMITER ';' CSV HEADER;

Create TABLE q1b AS
    SELECT n1 As year, sum(n7) As nbApplicants
    FROM importAnglais,
    Where n2 = "International" and n3 = "EU (excluding UK)"
    Group by n1;

COPY q1b TO './q1b.csv' DELIMITER ';' CSV HEADER;

Create TABLE q1c AS
    SELECT n1 As year, sum(n7) As nbApplicants
    FROM importAnglais,
    Where n2 = "International" and n3 = "Not EU"
    Group by n1;

COPY q1c TO './q1c.csv' DELIMITER ';' CSV HEADER;

Create TABLE q2 AS
    SELECT n1 As year, n3 As domicile, sum(n7) As nbApplicants
    FROM importAnglais,
    Where n3 = "UK" or n3 = "EU (excluding UK)" or n3 = "Not EU" or n3 = "Northern Ireland" or n3 = "Scotland" or n3 = "Wales" 
    Group by n1, n3;