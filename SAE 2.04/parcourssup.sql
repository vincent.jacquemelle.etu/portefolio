drop table IF EXISTS import,  Lieu,  Formation,  Etablissement,  Admission,  candidature CASCADE;
drop sequence if exists test, serial;
\! echo "Telechargement des données"
\! sleep 3

\! wget -O Données.csv https://data.enseignementsup-recherche.gouv.fr/api/explore/v2.1/catalog/datasets/fr-esr-parcoursup/exports/csv?lang=fr&timezone=Europe%2FBerlin&use_labels=true&delimiter=%3B & wait

\! echo "Création de la table import qui contient toutes les colonnes du fichier csv avec leur types respectifs"
\! sleep 3

CREATE TABLE import(
 n1 int ,  n2 varchar(343) ,  n3 varchar(343) ,  n4 varchar(343) ,  n5 varchar(343) ,  n6 varchar(343) ,  n7 varchar(343) ,  n8 varchar(343) ,  n9 varchar(343) ,  n10 varchar(343) ,  n11 varchar(343) ,  n12 varchar(343) ,  n13 varchar(343) ,  n14 varchar(343) ,  n15 varchar(343) ,  n16 varchar(343) ,  n17 varchar(343) ,  n18 int ,  n19 int ,  n20 int ,  n21 int ,  n22 varchar(343) ,  n23 int ,  n24 int ,  n25 int ,  n26 int ,  n27 int ,  n28 int ,  n29 int ,  n30 int ,  n31 int ,  n32 int ,  n33 int ,  n34 int ,  n35 int ,  n36 int ,  n37 varchar(343) ,  n38 varchar(343) ,  n39 int ,  n40 int ,  n41 int ,  n42 int ,  n43 int ,  n44 int ,  n45 int ,  n46 int ,  n47 int ,  n48 int ,  n49 int ,  n50 int ,  n51 FLOAT ,  n52 FLOAT ,  n53 FLOAT ,  n54 varchar(343) ,  n55 int ,  n56 int ,  n57 int ,  n58 int ,  n59 int ,  n60 int ,  n61 int ,  n62 int ,  n63 int ,  n64 int ,  n65 int ,  n66 FLOAT ,  n67 int ,  n68 int ,  n69 int ,  n70 varchar(343) ,  n71 varchar(343) ,  n72 int ,  n73 int ,  n74 FLOAT ,  n75 FLOAT ,  n76 FLOAT ,  n77 FLOAT ,  n78 FLOAT ,  n79 FLOAT ,  n80 FLOAT ,  n81 FLOAT ,  n82 FLOAT ,  n83 FLOAT ,  n84 FLOAT ,  n85 FLOAT ,  n86 FLOAT ,  n87 FLOAT ,  n88 FLOAT ,  n89 FLOAT ,  n90 FLOAT ,  n91 FLOAT ,  n92 FLOAT ,  n93 FLOAT ,  n94 FLOAT ,  n95 FLOAT ,  n96 FLOAT ,  n97 FLOAT ,  n98 FLOAT ,  n99 FLOAT ,  n100 FLOAT ,  n101 FLOAT ,  n102 varchar(343) ,  n103 varchar(343) ,  n104 varchar(343) ,  n105 varchar(343) ,  n106 varchar(343) ,  n107 varchar(343) ,  n108 varchar(343) ,  n109 varchar(343) ,  n110 varchar(343) ,  n111 varchar(343) ,  n112 varchar(343) ,  n113 varchar(343) ,  n114 varchar(343) ,  n115 varchar(343) ,  n116 varchar(343) ,  n117 varchar(343) ,  n118 varchar(343) 
);

\copy import from Données.csv delimiter ';' csv header;

\! echo "Creation des 5 tables du MCD"
\! sleep 3

create table Etablissement(
    eno serial PRIMARY KEY,  
    Code_UAI VARCHAR(100),  
    Nom varchar(500),  
    Academie varchar(100),  
    Statut varchar (100),  
    Coords varchar(500)
);

create table Lieu(
    lno serial PRIMARY KEY,  
    num_departement VARCHAR(100),  
    nom_region varchar(300),  
    nom_departement varchar(300),  
    commune varchar(300),  
    eno int,   
    Code_UAI VARCHAR(200),  
    FOREIGN KEY (eno) REFERENCES Etablissement(eno) ON DELETE CASCADE 
);


CREATE TABLE Admission(
    Ano serial PRIMARY KEY,  
    nbCandidates INT,  
    phasePrincipal int,  
    phaseComplementaire int,  
    procédurePrinc int,  
    avantBac int,  
    avantFinProcedurePrinc int,  
    boursiers_neoBac int,  
    neoBacGen int,  
    neoBacTechno int,  
    neoBacPro int,  
    autres int,    
    gen_mention int,  
    tech_mention int,  
    pro_mention int
);

Create TABLE candidature(
    Cno serial PRIMARY KEY,  
    Candidates int,  
    Gen int,  
    GenParEtablissement int,  
    TechParEtablissement int,  
    ProParEtablissement int,  
    AutreParEtablissement int,  
    BoursierGenParEtablissement int,  
    BoursierTechParEtablissement int,  
    BoursierProParEtablissement int,  
    Autres INT
);

create table Formation(
    Code_affForm VARCHAR(100) PRIMARY KEY,  
    Selectivite varchar(100),  
    typeF varchar(250),  
    Filiere varchar(350),  
    Domaine varchar(350),  
    FTD varchar(350),  
    eno int,   
    FOREIGN KEY (eno) REFERENCES Etablissement(eno) ON DELETE CASCADE
);

\! echo "Import des données de la tables import dans les 5 tables ventilés :"
\! sleep 3

INSERT INTO Etablissement (Code_UAI,  nom,  Academie,  Statut,  Coords) 
    SELECT DISTINCT(n3),  n4,  n8,  n2,  n17 
    FROM import;

create sequence test start 1;

Insert into Lieu (num_departement,  nom_region,  nom_departement,  commune,  Code_UAI) 
    Select distinct(n5),  n7,  n6,  n9,  n110 
    from import;

INSERT INTO Admission (nbCandidates,  phasePrincipal,  phaseComplementaire,  procédurePrinc,  avantBac,  avantFinProcedurePrinc,  boursiers_neoBac,  neoBacGen,  neoBacTechno,  neoBacPro,  autres, gen_mention, tech_mention, pro_mention) 
    SELECT n48,  n49,  n50,  n51,  n52,  n53,  n55,  n57,  n58,  n59,  n60,  n67, n68, n69 
    FROM import;

INSERT INTO candidature (Candidates,  Gen,  GenParEtablissement,  TechParEtablissement,  ProParEtablissement,  
BoursierGenParEtablissement,  BoursierTechParEtablissement,  BoursierProParEtablissement,  Autres) 
SELECT n20,  n23,  n39,  n41,  n43,  n45,  n40,  n42,  n44 from import;

ALTER TABLE Formation ADD cno INT;
ALTER TABLE Formation ADD FOREIGN KEY (cno) REFERENCES candidature(cno) ON DELETE CASCADE;
ALTER TABLE Formation ADD ano INT;
ALTER TABLE Formation ADD FOREIGN KEY (ano) REFERENCES Admission(ano) ON DELETE CASCADE;

Insert into Formation (Code_affForm,  Selectivite,  typeF,  Filiere,  Domaine,  FTD,  ano,  cno,  eno) 
    Select distinct(n110),  n11,  n12,  n14,  n15,  n16,  ano,  cno,  eno
    from import Inner join Admission 
        on nbCandidates = n20 and phasePrincipal = n49 and phaseComplementaire = n50 and procédurePrinc = n51 and avantBac = n52 and avantFinProcedurePrinc = n53 and boursiers_neoBac = n55 and neoBacGen = n57 and neoBacTechno = n58 and neoBacPro = n59 and autres = n60 and gen_mention = n67 and tech_mention = n68 and pro_mention = n69
        Inner join candidature 
            on Candidates = n20 and Gen = n23 and GenParEtablissement = n39 and TechParEtablissement = n41 and ProParEtablissement = n43 and BoursierGenParEtablissement = n45 and BoursierTechParEtablissement = n40 and BoursierProParEtablissement = n42 and candidature.Autres = n44
            Inner join Etablissement 
                on Code_UAI = n3 and nom = n4 and Academie = n8 and Statut = n2 and Coords = n17;

\! echo "Reponses au questions de l'exercices 2 :"
\! sleep 3

\! echo '1) Taille en octet du fichier récupéré :'
\! wc -c Données.csv
\! sleep 3

\! echo '2) Taille en octet de la table import :'
select pgotal_relation_size('import');
\! sleep 3

\! echo '3) Taille en octet de la somme des tables créées :'

\! sleep 3

select pgotal_relation_size('Etablissement')+pgotal_relation_size('Formation')+pgotal_relation_size('Lieu')+pgotal_relation_size('Admission')+pgotal_relation_size('candidature')as taille_sommeable;

\! echo '4) Exportation des tables ventilées dans le dossier "tablesExportée" :'
\! sleep 3

\copy Admission to 'tablesExportée/Admission.csv';
\copy candidature to 'tablesExportée/candidature.csv';
\copy Lieu to 'tablesExportée/Lieu.csv';
\copy Etablissement to 'tablesExportée/Etablissement.csv';
\copy Formation to 'tablesExportée/Formation.csv';

\! echo 'reponse à la question 4 :'
\! sleep 3

\! wc -c tablesExportée/Lieu.csv -c tablesExportée/Formation.csv -c tablesExportée/candidature.csv -c tablesExportée/Admission.csv -c tablesExportée/Etablissement.csv

\! echo "Suppression du fichier Données.csv"