class GenerateurSite extends Program {
   
    final char NEW_LINE = '\n';

    String[] rechercherValeur(String nomFichier) {
        String[] tabValeur = new String[5];
        int i = 0;
        int debutValeur = 0;
        int numeroCaseTab = 0;
        String txt = fileAsString(nomFichier);

        while (i < length(txt) && numeroCaseTab <= 4) {
            if (charAt(txt,i) == ':') {
                debutValeur = i + 2; // sert de marque page
            }

            if (charAt(txt,i) == '\n') {
                tabValeur[numeroCaseTab] = substring(txt,debutValeur,i);
                numeroCaseTab = numeroCaseTab + 1;
            }

            i = i + 1;
        }
        return tabValeur;
    }

    String template(String contenuSection) {      

        String htmlTemplate   = 
            "<!DOCTYPE html>" + NEW_LINE +
            "<html lang=\"fr\">" + NEW_LINE + 
            "  <head>" + NEW_LINE + 
            "    <title>Ordinateurs mythiques</title>" + NEW_LINE + 
            "    <meta charset=\"utf-8\">" + NEW_LINE +  
            "    <link rel=\"stylesheet\" type=\"text/css\" href=\"styles.css\">" + NEW_LINE +
            "  </head>" + NEW_LINE + 
            "  <body>" + NEW_LINE +
            "    <header>" + NEW_LINE +
            "      <h1>Ordinateurs mythiques</h1>" + NEW_LINE +
            "    </header>" + NEW_LINE +
            "    <nav>" + NEW_LINE +
            "      <ul>" + NEW_LINE +
            "        <li><a href=\"index.html\">Accueil</a></li>" + NEW_LINE +
            "        <li><a href=\"produit1.html\">Produit 1</a></li>" + NEW_LINE +
            "        <li><a href=\"produit2.html\">Produit 2</a></li>" + NEW_LINE +
            "        <li><a href=\"produit3.html\">Produit 3</a></li>" + NEW_LINE +
            "        <li><a href=\"produit4.html\">Produit 4</a></li>" + NEW_LINE +
            "        <li><a href=\"produit5.html\">Produit 5</a></li>" + NEW_LINE +
            "      </ul>" + NEW_LINE +
            "    </nav>" + NEW_LINE +
            "    <main>" + NEW_LINE +
            "      <section>" + NEW_LINE +
            contenuSection + NEW_LINE +
            "      </section>" + NEW_LINE +
            "    </main>" + NEW_LINE +
            "  </body>" + NEW_LINE + 
            "</html>";
            
        return htmlTemplate;
    }

    String templateContenu(String[] Valeur) {
    
// fonction qui creer la section pour les produits

    return ........; // ton retour de var
    }
    
    void algorithm() {
        String sectionIndex = .......;
// le section de ton index 
        String nomFichier = .......; // le nom de ton fichier;
        String nom;

        stringAsFile(........ , ........); // fonction pour creer un fichier qui prend en paramètre (<le nom du fichier>, <son cotenu>)

// boucle pour creer le html des produits

    }
}