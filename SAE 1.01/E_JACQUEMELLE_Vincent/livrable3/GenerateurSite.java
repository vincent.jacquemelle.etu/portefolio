class GenerateurSite extends Program {
   
    final char NEW_LINE = '\n';
    final int IDX_NOM = 0;
    final int IDX_DATE = 1;
    final int IDX_PRIX = 3;
    final int IDX_ENTREPRISE = 2;
    final int IDX_DESCRIPTION = 4;

    String[] rechercherValeur(String chaine) {
        final String[] TabCleValeur = new String[]{"nom : ", "date : ", "entreprise : ", "prix : ", "description : "};
        String[] TabValeur = new String[5];
        String cle = "";

        for(int i = 0; i < length(TabCleValeur); i=i+1) {
            cle = TabCleValeur[i];
            
            int indice = 0;
            while (indice < length(chaine) && indice+length(cle) < length(chaine) && 
                !equals(cle, substring(chaine, indice, indice+length(cle)))) {
                indice = indice + 1;
            }
            if (indice < length(chaine)-length(cle)) {
                int indiceRetourLigne = indice;
                while (indiceRetourLigne < length(chaine) && charAt(chaine, indiceRetourLigne) != NEW_LINE) {
                    indiceRetourLigne = indiceRetourLigne + 1;
                }
                TabValeur[i] = substring(chaine, indice+length(cle), indiceRetourLigne);
                
            }
        }

        return TabValeur;
    }

    String template(String contenuSection, String navListe) {      

        String htmlTemplate   = 
            "<!DOCTYPE html>" + NEW_LINE +
            "<html lang=\"fr\">" + NEW_LINE + 
            "  <head>" + NEW_LINE + 
            "    <title>Ordinateurs mythiques</title>" + NEW_LINE + 
            "    <meta charset=\"utf-8\">" + NEW_LINE +  
            "    <link rel=\"stylesheet\" type=\"text/css\" href=\"styles.css\">" + NEW_LINE +
            "  </head>" + NEW_LINE + 
            "  <body>" + NEW_LINE +
            "    <header>" + NEW_LINE +
            "      <h1>Ordinateurs mythiques</h1>" + NEW_LINE +
            "    </header>" + NEW_LINE +
            "    <nav>" + NEW_LINE +
            "      <ul>" + NEW_LINE +
            navListe +
            "      </ul>" + NEW_LINE +
            "    </nav>" + NEW_LINE +
            "    <main>" + NEW_LINE +
            "      <section>" + NEW_LINE +
            contenuSection + NEW_LINE +
            "      </section>" + NEW_LINE +
            "    </main>" + NEW_LINE +
            "  </body>" + NEW_LINE + 
            "</html>" + NEW_LINE;
            
        return htmlTemplate;
    }

    String templateContenu(String[][] Valeur, int Ligne) {
        String Nom = Valeur[Ligne][IDX_NOM];
        String Date = Valeur[Ligne][IDX_DATE];
        String Entreprise = Valeur[Ligne][IDX_ENTREPRISE];
        String Prix = Valeur[Ligne][IDX_PRIX];
        String description = Valeur[Ligne][IDX_DESCRIPTION];
        String contenu = 
            "        <h2>" + Nom + " (" + Entreprise + ")" + "</h2>" + NEW_LINE +
            "        <h3>" + Prix + " (Sortie en " + Date + ")" + "</h3>" + NEW_LINE +
            "        <p>" + NEW_LINE +
            description + NEW_LINE + 
            "        </p>";
        
        return contenu;
    }

    String templateNav(String[] ListeProduit, String[][] Valeur, int numProduit) {
        String NavContenu = "        <li><a href=\"index.html\">Accueil</a></li>" + NEW_LINE;
        int nFois = 5;
        final int nbProduit = length(ListeProduit);
        int produitX_plusUn = numProduit + 1;
        int produitX_plusDeux = numProduit + 2;
        int produitX_plusUTrois = numProduit + 3;
        int produitX_plusQuatre = numProduit + 4;
        numProduit = numProduit - 1;
        

        if(produitX_plusQuatre > nbProduit) {
            nFois = 4;
        }
        if(produitX_plusUTrois > nbProduit) {
            nFois = 3;
        }
        if(produitX_plusDeux > nbProduit) {
            nFois = 2;
        }
        if(produitX_plusUn > nbProduit) {
            nFois = 1;
        }
        
        

        for(int i = 0; i<nFois; i=i+1) {
            NavContenu = NavContenu + "        <li><a href=\"produit" + (numProduit+1) + ".html\">" + Valeur[numProduit][IDX_NOM] + "</a></li>" + NEW_LINE;
            numProduit = numProduit + 1;
        }        
        return NavContenu;
    }

    String[][] chargerProduits(String repertoire, String prefixe) {

        String[][] tabDonnee = new String[length(getAllFilesFromDirectory(repertoire))][5];
        String[] TabTempo;
        int nbProduit = length(getAllFilesFromDirectory(repertoire));
        int numProduit;

        for(int i = 0; i<nbProduit; i=i+1) {
            numProduit = i + 1;
            TabTempo = rechercherValeur(fileAsString(repertoire + prefixe + numProduit + ".txt"));
            for(int j = 0; j<5; j=j+1) {
                tabDonnee[i][j] = TabTempo[j];
                
            }
        }
        return tabDonnee;
    }

    String[] trieProduit(String[] tabNonRange) {
        String[] tabRange = new String[length(tabNonRange)];
        int numeroProduit = 1;
        int numeroCase = 0;
        String NomProduit = "";

        while(numeroCase<length(tabNonRange)) {
            NomProduit = "produit" + numeroProduit;

            if (equals(NomProduit, tabNonRange[numeroCase])) {
                tabRange[numeroProduit-1] = tabNonRange[numeroCase];
                numeroProduit = numeroProduit + 1;
                println(tabRange[numeroProduit-1]);
            }
            else {
                numeroCase = numeroCase + 1;
            }
        }
        return tabRange;
    }
    
    void algorithm() {
        String ContenuIndex = 
            "        <h2>Tout ce que vous avez toujours voulu savoir sur les vieux ordis sans jamais avoir osé le demander !</h2>" + NEW_LINE +
            "          <p>" + NEW_LINE +
            "Bienvenue dans le musée virtuel d'ordinateurs mythiques de l'histoire de l'informatique. Vous trouverez ici des éléments sur quelques machines qui ont marqué l'histoire de l'informatique que cela soit par leurs caractéristiques techniques ou l'impact commercial qu'elles ont eu et qui ont contribué au développement du secteur informatique." + NEW_LINE +
            "          </p>";
        String NavIndex = 
            "        <li><a href=\"index.html\">Accueil</a></li>" + NEW_LINE +
            "        <li><a href=\"produit1.html\">Altair 8800</a></li>" + NEW_LINE +
            "        <li><a href=\"produit2.html\">NeXT Computer</a></li>" + NEW_LINE +
            "        <li><a href=\"produit3.html\">Sinclair ZX Spectrum</a></li>" + NEW_LINE +
            "        <li><a href=\"produit4.html\">Amiga 1000</a></li>" + NEW_LINE +
            "        <li><a href=\"produit5.html\">ENIAC</a></li>" + NEW_LINE;
        String nomFichier = "output/index.html";
        String nom;

        stringAsFile(nomFichier, template(ContenuIndex, NavIndex));

        String[] Produits = trieProduit(getAllFilesFromDirectory("data/"));
        String[][] DonneeProduits = chargerProduits("data/", "produit");
        int nbProduits = length(Produits);
        int numeroProduit = 1;

        for(int i = 1; i<=nbProduits; i=i+1) {
            numeroProduit = i;
            nomFichier = "output/produit" + numeroProduit + ".html";
            stringAsFile(nomFichier,template(templateContenu(DonneeProduits, i-1), templateNav(Produits, DonneeProduits, numeroProduit)));
        }
    }
}