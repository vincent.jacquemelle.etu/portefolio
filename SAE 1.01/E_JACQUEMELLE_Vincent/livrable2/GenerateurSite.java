class GenerateurSite extends Program {
   
    final char NEW_LINE = '\n';

    String[] rechercherValeur(String nomFichier) {
        String[] tabValeur = new String[5];
        int i = 0;
        int debutValeur = 0;
        int numeroCaseTab = 0;
        String txt = fileAsString(nomFichier);

        while (i < length(txt) && numeroCaseTab <= 4) {
            if (charAt(txt,i) == ':') {
                debutValeur = i + 2; // sert de marque page
            }

            if (charAt(txt,i) == '\n') {
                tabValeur[numeroCaseTab] = substring(txt,debutValeur,i);
                numeroCaseTab = numeroCaseTab + 1;
            }

            i = i + 1;
        }
        return tabValeur;
    }

    String template(String contenuSection) {      

        String htmlTemplate   = 
            "<!DOCTYPE html>" + NEW_LINE +
            "<html lang=\"fr\">" + NEW_LINE + 
            "  <head>" + NEW_LINE + 
            "    <title>Ordinateurs mythiques</title>" + NEW_LINE + 
            "    <meta charset=\"utf-8\">" + NEW_LINE +  
            "    <link rel=\"stylesheet\" type=\"text/css\" href=\"styles.css\">" + NEW_LINE +
            "  </head>" + NEW_LINE + 
            "  <body>" + NEW_LINE +
            "    <header>" + NEW_LINE +
            "      <h1>Ordinateurs mythiques</h1>" + NEW_LINE +
            "    </header>" + NEW_LINE +
            "    <nav>" + NEW_LINE +
            "      <ul>" + NEW_LINE +
            "        <li><a href=\"index.html\">Accueil</a></li>" + NEW_LINE +
            "        <li><a href=\"produit1.html\">Produit 1</a></li>" + NEW_LINE +
            "        <li><a href=\"produit2.html\">Produit 2</a></li>" + NEW_LINE +
            "        <li><a href=\"produit3.html\">Produit 3</a></li>" + NEW_LINE +
            "        <li><a href=\"produit4.html\">Produit 4</a></li>" + NEW_LINE +
            "        <li><a href=\"produit5.html\">Produit 5</a></li>" + NEW_LINE +
            "      </ul>" + NEW_LINE +
            "    </nav>" + NEW_LINE +
            "    <main>" + NEW_LINE +
            "      <section>" + NEW_LINE +
            contenuSection + NEW_LINE +
            "      </section>" + NEW_LINE +
            "    </main>" + NEW_LINE +
            "  </body>" + NEW_LINE + 
            "</html>";
            
        return htmlTemplate;
    }

    String templateContenu(String[] Valeur) {
    
    String Titre = Valeur[0];
    String Date = Valeur[1];
    String Entreprise = Valeur[2];
    String Prix = Valeur[3];
    String description = Valeur[4];
    String contenu = 
        "        <h2>" + Titre + " (" + Entreprise + ")" + "</h2>" + NEW_LINE +
        "        <h3>" + Prix + " (Sortie en " + Date + ")" + "</h3>" + NEW_LINE +
        "        <p>" + NEW_LINE +
        description + NEW_LINE + 
        "        </p>";
    
    return contenu;
    }
    
    void algorithm() {
        String index = 
            "        <h2>Tout ce que vous avez toujours voulu savoir sur les vieux ordis sans jamais avoir osé le demander !</h2>" + NEW_LINE +
            "          <p>" + NEW_LINE +
            "Bienvenue dans le musée virtuel d'ordinateurs mythiques de l'histoire de l'informatique. Vous trouverez ici des éléments sur quelques machines qui ont marqué l'histoire de l'informatique que cela soit par leurs caractéristiques techniques ou l'impact commercial qu'elles ont eu et qui ont contribué au développement du secteur informatique." + NEW_LINE +
            "          </p>";
        String nomFichier = "output/index.html";
        String nom;

        stringAsFile(nomFichier, template(index));

        for (int i=1; i <= 5; i=i+1) {
            nomFichier = "output/produit" + i + ".html";
            nom = "data/produit" + i + ".txt";
            stringAsFile(nomFichier, template(templateContenu(rechercherValeur(nom))));
            
        }

    }
}