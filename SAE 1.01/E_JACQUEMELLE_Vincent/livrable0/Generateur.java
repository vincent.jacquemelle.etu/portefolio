class Generateur extends Program {

    String fileAsString(String filename) {
        extensions.File f = new extensions.File(filename);
        String content = "";
        while (ready(f)) {
            content = content + readLine(f) + '\n';
        }
        return content;
    }
    
    final char NEW_LINE = '\n';

    
    void algorithm() {
        final String FILENAME = "data/Zen Of Python.txt";
        final String TITLE    = substring(FILENAME, 5, length(FILENAME)-3);
        final String CONTENT  = fileAsString(FILENAME);
        String htmlTemplate   = 
            "<!DOCTYPE html>" + NEW_LINE +
            "<html lang=\"fr\">" + NEW_LINE + 
            "  <head>" + NEW_LINE + 
            "    <title> " + TITLE + " </title>" + NEW_LINE + 
            "    <meta charset=\"utf-8\">" + NEW_LINE +  
            "  </head>" + NEW_LINE + 
            "  <body>" + NEW_LINE + 
            "    <p>" + NEW_LINE +
            CONTENT + 
            "    </p>" + NEW_LINE + 
            "  </body>" + NEW_LINE + 
            "</html>";
        
        println(htmlTemplate);
    }
}
