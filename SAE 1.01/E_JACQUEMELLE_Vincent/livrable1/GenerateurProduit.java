class GenerateurProduit extends Program {
   
    final char NEW_LINE = '\n';

    String[] rechercherValeur(String nomFichier) {
        String[] tabValeur = new String[5];
        int i = 0;
        int debutValeur = 0;
        int numeroCaseTab = 0;
        String txt = fileAsString(nomFichier);

        while (i < length(txt) && numeroCaseTab <= 4) {
            if (charAt(txt,i) == ':') {
                debutValeur = i + 2; // sert de marque page
            }

            if (charAt(txt,i) == '\n') {
                tabValeur[numeroCaseTab] = substring(txt,debutValeur,i);
                numeroCaseTab = numeroCaseTab + 1;
            }

            i = i + 1;
        }
        return tabValeur;
    }

    void template(String nomFichier) {        

        String[] tabValeur = rechercherValeur(nomFichier);

        String Titre = tabValeur[0];
        String Date = tabValeur[1];
        String Entreprise = tabValeur[2];
        String Prix = tabValeur[3];
        String Contenu  = tabValeur[4];
        String htmlTemplate   = 
            "<!DOCTYPE html>" + NEW_LINE +
            "<html lang=\"fr\">" + NEW_LINE + 
            "  <head>" + NEW_LINE + 
            "    <title>" + Titre + "</title>" + NEW_LINE + 
            "    <meta charset=\"utf-8\">" + NEW_LINE +  
            "  </head>" + NEW_LINE + 
            "  <body>" + NEW_LINE +
            "    <h1>" + Titre + " (" + Entreprise + ")" + "</h1>" + NEW_LINE +
            "    <h2>" + Prix + " (Sortie en " + Date + ")" + "</h2>" + NEW_LINE +
            "    <p>" + NEW_LINE +
            Contenu + NEW_LINE + 
            "    </p>" + NEW_LINE + 
            "  </body>" + NEW_LINE + 
            "</html>";
            
        println(htmlTemplate);
    }

    
    void algorithm() {
        // mettre le if(fileExiste) ici
        if (fileExist(argument(0))) {

            template(argument(0));
        }
        else {
            error("Le fichier " + argument(0) + " n'existe pas");
        }
    }
}